TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ../diseqc.c \
    ../kb.c \
    ../tune-s2.c

include(deployment.pri)
qtcAddDeployment()

DISTFILES += \
    ../atsc.sh \
    ../menu.sh \
    ../README.md \
    ../Makefile

HEADERS += \
    ../diseqc.h \
    ../kb.h \
    ../tune-s2.h

LIBS += -Wl,-rpath, -l:libgps.so -l:libpthread.so
#libgps.so.20 => /usr/lib/i386-linux-gnu/libgps.so.20 (0xb7732000)
#libpthread.so.0 => /lib/i386-linux-gnu/libpthread.so.0 (0xb7715000
